#Home Insurance Test

This is a small frontend application where a user can choose between one of three home insurance packages and can send a form with some of their information.

This page is hosted on Netlify at this address: https://main--rainbow-axolotl-0df824.netlify.app
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

I decided To route the form information through the backend after validation. The api/sendform function takes care of establishing a connection to the example.com API endpoint and sending the information.


## TechStack
 - React
 - Typescript
 - Next.js
 - CSS3
 - HTML5
 - axios
 - ESLint
 - Yarn
