import type { NextPage } from 'next'
import { prepareServerlessUrl } from 'next/dist/server/base-server'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import React from 'react'

const Home: NextPage = () => {

  // USER FORM DATA
  const [selectedPlan, setSelectedPlan] = React.useState(0);
  const [genderval, setGenderVal] = React.useState("Select from below");
  const [email, setEmail] = React.useState("");
  const [age, setAge] = React.useState(18);

  // EMAIL REGEX VALIDATOR 
  const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  // ERROR HANDLERS
  const [selectedPlanError, setSelectedPlanError] = React.useState("");
  const [gendervalError, setGenderValError] = React.useState("");
  const [emailError, setEmailError] = React.useState("");
  const [ageError, setAgeError] = React.useState("");

  // SUCCESS MESSAGE TOGGLE
  const [showSuccessMess, setShowSuccessMess] = React.useState(false);

  //FORM INPUT DATA
  const insurancePlans = [
  {packageId: 1, type: "Proteco", description: "Our most affordable package. Your personal belongings will be covered up to 15k$. This is perfect if you own a few belongings.", price: "12.50" },
  {packageId: 2, type: "Umbrella", description: "Our most popular package. Your personal belongings will be covered up to 30k$. This package also includes a free water sensor to detect a water leak in your home.", price: "35.73" },
  {packageId: 3, type: "Thor", description: "Nothing but the best. Your personal belongings will be covered up to 100k$. It even includes a protection against an alien invasion.", price: "79.30" }
  ]
  const genderOptions =[
    {id:1,value:"Male"},
    {id:2,value:"Female"},
    {id:3,value:"Transgender"},
    {id:4,value:"Non-Binary/Non-Conforming"},
    {id:5,value:"Prefer not to Respond"},
  ]

  //Validates the form data and then sends it to the backend so it can be safely 
  //sent to the external API
  const validateForm = React.useCallback(async () => {
      // RESET ERRORS
      setSelectedPlanError("");
      setGenderValError("");
      setEmailError("");
      setAgeError("");

      //validate inputs
      let formError = false;
      let currentPlan = insurancePlans.find(plan => {
        return plan.packageId == selectedPlan ? plan.type : null;
      });

      if (!currentPlan) {
        setSelectedPlanError("Please Choose a plan."); 
        formError = true;
      }
  
      if (genderval == "Select from below") {
        setGenderValError("Please select an option.");
        formError = true;
      }
 
      if (! emailPattern.test(email.toLocaleLowerCase())) {
        setEmailError("Please type in a valid email.");
        formError = true;
      }

      if(age < 18) {
        setAgeError("You must be at least 18 years old.");
        formError = true;
      }

      //if all inputs are valid, send the form request
      if(!formError) {
        
        let response = await fetch("api/sendform", {
          method: "POST",
          body: JSON.stringify({
            currentPlan,
            genderval,
            email,
            age
          }),
          headers: {
            "Content-Type": "application/json"
          }
        });
        const {success} = await response.json();
        if (success) {
          // if the call is succesful, show the user a success message
            setShowSuccessMess(true);
        }
      }
  },[selectedPlan, genderval, email, age]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Home Insurance</title>
        <meta name="description" content="Protect yourself with these comprehensive home insurance plans" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
         {/* HEADER AND DESCRIPTION */}
        <h1 className={styles.title}>
          Home Insurance
        </h1>
        <p className={styles.description}>
          Protect yourself with these comprehensive home insurance plans
        </p>

        {/* INSURANCE PLAN CARDS */}
        <div className={styles.grid}>
            {insurancePlans.map(plan => {
              return (
                <button  key={plan.packageId} className={selectedPlan == plan.packageId ? styles.selectedcard  : styles.card } onClick={() => setSelectedPlan(plan.packageId)}>
                  <h1>{plan.type} Insurance</h1>
                  <p>{plan.description}</p>
                  <h2>$ {plan.price}</h2><h3>/month</h3>
                </button>

              );
            })}
        </div>
        {selectedPlanError != "" ? (<span className={styles.formerror}>{selectedPlanError}</span>): null}
         {/* SUCCESS MESSAGE CARD */}
         {showSuccessMess ? (<div className={styles.successcard}>
          <h2>Your request has been succesfully submitted. One of our representatives will contact you within the following days</h2>
        </div>) : null}

        {/* USER DATA FORM */}
        <div className={styles.form}>
          <h3 className={styles.forminstructions}>Fill out the form once you have selected a package</h3>
          <label htmlFor="first">email:</label>
          <input type="email" id="email" name="email" value={email}  onChange={(e) => setEmail(e.target.value)} />
          {emailError != "" ? (<span className={styles.formerror}>{emailError}</span>) : null}
          <label htmlFor="age">How old are you:</label>
          <input type="number"
           id="age"
           name="age"
           required
           value={age}
           onChange={(e) => setAge(Number(e.target.value))}
           />
           {ageError != "" ? (<span className={styles.formerror}>{ageError}</span>) : null}
          <label htmlFor="gender">What is your gender:</label>
          <select value={genderval} id="gender" name="gender" required onChange={(e) => setGenderVal(e.target.value)} >
            <option value="Select from below" selected hidden>Select from below</option>
            {genderOptions.map(option => {
              return (
                <option key={option.id} value={option.value}>{option.value}</option>
              );
            })}
          </select>
          {gendervalError != "" ? (<span className={styles.formerror}>{gendervalError}</span>) : null}
          <button onClick={() => validateForm()} className={styles.button}>Submit</button>
        </div>
      </main>

      <footer className={styles.footer}>
        Made by Jhonatan Orjuela
      </footer>
    </div>
  )
}

export default Home
