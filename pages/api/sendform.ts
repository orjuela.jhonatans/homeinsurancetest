// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios';
type Data = {
  success: boolean
}

//POST /api/sendform
const sendform = async(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) => {
  
  const {currentPlan, genderVal, email, age} = req.body;
  let body = {
    insurancePlan : currentPlan,
    gender : genderVal,
    email : email,
    age : age,
  }
  let responseSuccess = false;
  axios.post("example.com", body).then(response => {
    //We have to fake the response here as example.com does not exist
    // In a real life scenario we would take response.data and evaluate
    // The response from the API calll
  })
  responseSuccess = true;
  res.status(200).json({ success: responseSuccess })
}

export default sendform;